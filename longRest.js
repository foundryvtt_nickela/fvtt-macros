//requires about-time
game.Gametime.advanceTime({hours: 8});

let lr = game.Gametime.DTNow().longDate();

ChatMessage.create({
  flavor: `  The party takes a long rest. Eight hours pass...`,
  content: `<img src=img/uploads/iconLongRest.png style="width:25%"/><br>
  The time is... <br>
  <b>DATE:</b> ${lr.date}<br>
  <b>TIME:</b> ${lr.time}`
});

game.Gametime._save(true);
