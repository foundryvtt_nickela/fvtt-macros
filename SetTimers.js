//requires about-time
let applyChanges = false;
new Dialog({
  title: "Duration Configuration",
  content: `
    <form>
      <div class="form-group">
        <label>Spell or Effect:</label>
        <input type="text" id="eName" name="eName">
      </div>
      <div class="form-group"
        <label>Duration in Minutes:</label>
        <input type="number" id="eNumber" name="eNumber" min="0" step="1">
      </div>
    </form>
    `,
  buttons: {
    yes: {
      icon: "<i class='fas fa-check'></i>",
      label: "Apply Changes",
      callback: () => applyChanges = true
    },
    no: {
      icon: "<i class='fas fa-times'></i>",
      label: "Cancel Changes"
    },
  },
  default: "yes",
  close: html => {
    if (applyChanges) {
      let effectName = html.find('[name="eName"]')[0].value || "none";
      let effectDuration = html.find('[name="eNumber"]')[0].value * 60 || "none";
      let tokenSpeaker = ChatMessage.getSpeaker(token._id);
      let effectMessage = `
      <img src="${token.data.img}" style="width:10%"/> <i>Duration Set...</i>
      <p><b>Effect or Spell:</b> ${effectName}</p>
      <p><b>Duration:</b> ${effectDuration/60} minute(s)</p>
      `;
      let expireMessage = `
      <img src="${token.data.img}" style="width:10%"/>
      <p>${effectName} set by ${tokenSpeaker.alias} expired...
      `;
      ChatMessage.create({
        speaker: tokenSpeaker,
        content: effectMessage,
      }),
      Gametime.reminderIn({seconds: effectDuration}, `${expireMessage}`, tokenSpeaker.alias),
      console.log("FVTT Macro |",effectName,"duration created for",tokenSpeaker.alias)
      console.log("Foundry VTT | current about-time queue:");
      game.Gametime.queue();
    }
  }
}).render(true);

game.Gametime._save(true);
