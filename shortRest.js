//requires about-time
game.Gametime.advanceTime({minutes: 30});

let sr = game.Gametime.DTNow().longDate();

ChatMessage.create({
  flavor: `  The party stops to catch their breath. Thirty minutes pass...`,
  content: `<img src=img/uploads/iconShortRest.png style="width:25%"/><br>
  The time is... <br>
  <b>DATE:</b> ${sr.date}<br>
  <b>TIME:</b> ${sr.time}`
});

game.Gametime._save(true);
